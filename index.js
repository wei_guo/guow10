#!/usr/bin/env node

//index Routes File

'use strict'

/* MODULE IMPORTS */
const Koa = require('koa')
const views = require('koa-views')
const staticDir = require('koa-static')
const session = require('koa-session')
const cors = require('@koa/cors')
const koaBody = require('koa-body')
const Router = require('koa-router')
const dbName = 'website.db'

/* IMPORT CUSTOM MODULES */
const User = require('./routes/user')
const Item = require('./routes/item')
const Tags = require('./routes/tag')
const Tag = require('./modules/tag')
const Items = require('./modules/item')
const Email = require('./routes/email')

const app = new Koa()
const router = new Router()

/* CONFIGURING THE MIDDLEWARE */
app.use(koaBody({ multipart: true, uploadDir: '.' }))
app.keys = ['darkSecret']
app.use(staticDir('public'))
app.use(session(app))
app.use(cors())
app.use(views(`${__dirname}/views`, { extension: 'handlebars' }, { map: { handlebars: 'handlebars' } }))

const defaultPort = 8080
const port = process.env.PORT || defaultPort

/**
 * The secure home page.
 *
 * @name Home Page
 * @route {GET} /
 */
router.get('/', async ctx => {
	try {
		// extract the data from the database
		const tag = await new Tag()
		const tagList = await tag.getTagList()
		const items = await new Items(dbName)
		const itemList = await items.getItemListByTag(0)
		// get data to init homepage
		for (let i = 0; i < itemList.length; i++) {
			itemList[i].itemImg = `/itemImg/${itemList[i].title}.${itemList[i].imgType}`
			itemList[i].id = `/item?id=${itemList[i].id}`
		}
		for (let i = 0; i < tagList.length; i++) {
			tagList[i].id = `/create?id=${tagList[i].id}`
		}
		//set up ctx for image
		ctx.type = 'image/png'
		ctx.status = 200
		// render homePage
		await ctx.render('index', {
			authorised: ctx.session.authorised,
			userId: `user?id=${ctx.session.userId}`,
			tagList,
			itemList,
		})
	} catch (err) {
		await ctx.render('error', { message: err.message })
	}
})

/**
 * The secure home page.
 *
 * @name Home Page
 * @route {GET} /index
 */
router.get('/index', async ctx => {
	// redirect to the home page
	ctx.redirect('/')
})

app.use(User.routes())
app.use(Item.routes())
app.use(Tags.routes())
app.use(router.routes())
app.use(Email.routes())
module.exports = app.listen(port, async() => console.log(`listening on port ${port}`))
