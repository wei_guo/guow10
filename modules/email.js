
'use strict'

const sqlite = require('sqlite-async')
const nodemailer = require('nodemailer')
const emailConfig = require('../email.json')

module.exports = class Email {

    constructor(dbName = ':memory:') {
        return (async() => {
            this.db = await sqlite.open(dbName)
            // we need this table to store the email history
            const sql = `CREATE TABLE IF NOT EXISTS email (
                id INTEGER PRIMARY KEY AUTOINCREMENT, 
                fromUser TEXT NOT NULL,
                toUser TEXT NOT NULL,
                sentByType INT NOT NULL,
                itemId INT NOT NULL,
                willingItem TEXT NOT NULL
                );`
            await this.db.run(sql)

            return this
        })()
    }

    checkItem(values) {
        for (let i = 0; i < values.length; i++) {
            if (values[i].length === 0)
                return false
        }
        return true
    }

    async sendEmail(sentUser, hostUser, itemData, willingItem, tagData) {
        // check parameters
        const sendData = [sentUser, hostUser, itemData, willingItem, tagData]
        for (let i = 0; i < sendData.length; i++) {
            if (sendData[i] === null || sendData[i] === undefined) throw new Error('email needs parameters error')
        }
        // send the email
        // SMTP server
        const mailTransport = nodemailer.createTransport(emailConfig)
        let tagList = ''
        for (let i=0; i<tagData.length; i++)
            tagList += `${tagData[i].tag} `
        // context
        const mailOptions = {
            from: emailConfig.auth.user,
            to: hostUser.email,
            subject: 'exchange item offer',
            html: `<p>Mr/Ms ${hostUser.user}</p> 
            user Mr/Ms <a style="color:red"> ${sentUser.user} <a/>
            wants to use <a style="color:red">${willingItem} </a>
            to exchange your item <a style="color:red">${itemData.title}</a></p>
            <h4>The item details</h4>
            <p>title: <a style="color:red">${itemData.title}</a></p>
            <p>description: <a style="color:red">${itemData.description}</a></p>
            <p>a item list that you want to swap by this item: <a style="color:red"> ${tagList}</a></p>
            `
        }
        try {
            mailTransport.sendMail(mailOptions)
            // save the email log to database
            await this.saveEmailLog(sentUser.user, hostUser.user, 0, itemData.id, willingItem)
            // if (!data) throw new Error('The email log saved error')
        } catch (err) {
            throw err
        }
        return true
    }

    // sentByType: 0 = make an offer by users, 1 = auto suggestion email by the system
    async saveEmailLog(fromUser, toUser, sentByType, itemId, willingItem) {
        try {
            // check parameters
            const values = [fromUser, toUser, sentByType, itemId, willingItem]
            if (this.checkItem(values) === false) throw new Error('missing the values')
            const sql = `INSERT INTO email(fromUser, toUser, sentByType, itemId, willingItem)
                VALUES("${fromUser}", "${toUser}", "${sentByType}", "${itemId}", "${willingItem}")`
            const data = await this.db.run(sql)
            return data.lastID
        } catch (err) {
            throw err
        }
    }
}
