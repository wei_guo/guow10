
'use strict'

const bcrypt = require('bcrypt-promise')
const mime = require('mime-types')
const sqlite = require('sqlite-async')
const Sharp = require('sharp')
const saltRounds = 10

module.exports = class User {

	constructor(dbName = ':memory:') {
		return (async() => {
			this.db = await sqlite.open(dbName)
			// we need this table to store the user accounts
			const sql = `CREATE TABLE IF NOT EXISTS users (
				id INTEGER PRIMARY KEY AUTOINCREMENT, 
				user TEXT NOT NULL, 
				pass TEXT NOT NULL, 
				email TEXT NOT NULL
				);`
			await this.db.run(sql)
			return this
		})()
	}

	async getUserById(id) {
		if (id === null || id === undefined) throw new Error('user Id is error')
		try {
			const sql = `SELECT * FROM users WHERE id=${id}`
			const data = await this.db.get(sql)
			if (!data) throw new Error('get user by Id error')
			return data
		} catch (err) {
			throw err
		}
	}

	async register(user, pass, email) {
		try {
			if (user.length === 0) throw new Error('missing username')
			if (pass.length === 0) throw new Error('missing password')
			if (email.length === 0) throw new Error('missing email')
			let sql = `SELECT COUNT(id) as records FROM users WHERE user="${user}";`
			const data = await this.db.get(sql)
			if (data.records !== 0) throw new Error(`username "${user}" already in use`)
			pass = await bcrypt.hash(pass, saltRounds)
			sql = `INSERT INTO users(user, pass, email) VALUES("${user}", "${pass}", "${email}")`
			await this.db.run(sql)
			return true
		} catch (err) {
			throw err
		}
	}

	async uploadPicture(path, mimeType, username) {
		try {
			if (path.length === 0 || path === undefined)
				throw new Error('image path err')
			if (mimeType.length === 0 || mimeType === undefined)
				throw new Error('image format err')
			const extension = await mime.extension(mimeType)
			const pic = `public/itemImg/${username}.${extension}`
			const sizeX = 300
            const sizeY = 200
            await Sharp(path).resize(sizeX, sizeY).toFile(pic)
			return true
		} catch (err) {
			throw err
		}
	}

	async login(username, password) {
		try {
			let sql = `SELECT count(id) AS count FROM users WHERE user="${username}";`
			const records = await this.db.get(sql)
			if (!records.count) throw new Error(`username "${username}" not found`)
			sql = `SELECT pass FROM users WHERE user = "${username}";`
			let record = await this.db.get(sql)
			const valid = await bcrypt.compare(password, record.pass)
			if (valid === false) throw new Error(`invalid password for account "${username}"`)
			sql = `SELECT id from users WHERE user= "${username}"`
			record = await this.db.get(sql)
			return record
		} catch (err) {
			throw err
		}
	}
}
