
'use strict'

const sqlite = require('sqlite-async')

module.exports = class Tag {

	constructor(dbName = ':memory:') {
		return (async() => {
			this.db = await sqlite.open(dbName)
			// we need this table to store the item tag
			const sql = `CREATE TABLE IF NOT EXISTS tags (
                id INTEGER PRIMARY KEY AUTOINCREMENT, 
                tag TEXT NOT NULL
                );`
            await this.db.run(sql)
            await this.initTags()
			return this
		})()
    }

    async initTags() {
        try {
            //init Tags data
            let sql = 'SELECT * FROM tags'
            const data = await this.db.all(sql)
            if (!data.length) {
                const tags = [
                    'shoes', 'sock', 'pants', 'clothes',
                    'Underwear(woman)', 'Underwear(male)',
                    'cup', 'books', 'Handicrafts'
                ]
                for (let i = 0; i < tags.length; i++) {
                    sql = `INSERT INTO tags(tag) VALUES("${tags[i]}")`
                    await this.db.run(sql)
                }
            }
            return true
        } catch(err) {
            throw err
        }
    }

    async getTagList() {
        try {
            const sql = 'SELECT * FROM tags'
            const data = await this.db.all(sql)
            if (!data.length) throw new Error('there is no tag')
            return data
        } catch (err) {
            throw err
        }
    }

    async getTagById(tagId) {
        //check parameter
        if (tagId === null || tagId === undefined) throw new Error('parameter tagId is error')
        try {
            const sql = `SELECT * FROM tags where id=${tagId}`
            const data = await this.db.get(sql)
            return data
        } catch (err) {
            throw err
        }
    }

    async getTagByName(tag) {
        //check parameter
        if (tag === null || tag === undefined) throw new Error('parameter tag is error')
        try {
            const sql = `SELECT * FROM tags where tag="${tag}"`
            const data = await this.db.get(sql)
            if (!data) return null
            return data
        } catch (err) {
            throw err
        }
    }

    async createTag(tag) {
        //check parameter
        if (tag === null || tag === undefined) throw new Error('parameter tag is error')
        try {
            const sql = `INSERT INTO tags(tag) VALUES("${tag}")`
            const data = await this.db.run(sql)
            return data.lastID
        } catch (err) {
            throw err
        }
    }

    async getItemSwapTagList(swapList) {
        if (swapList === null || swapList === undefined) throw new Error('parameter swapList is error')
        try {
            const idList = swapList.split(',')
            const swapForItems = []
            for (let i = 0; i < idList.length; i++) {
                const sql = `SELECT * FROM tags where id=${idList[i]}`
                const data = await this.db.get(sql)
                swapForItems.push(data)
            }
            return swapForItems
        } catch (err) {
            throw err
        }
    }

}
