
'use strict'

const sqlite = require('sqlite-async')
const Sharp = require('sharp')
const mime = require('mime-types')

module.exports = class Item {

    constructor(dbName = ':memory:') {
        return (async() => {
            this.db = await sqlite.open(dbName)
            // we need this table to store the Item
            const sql = `CREATE TABLE IF NOT EXISTS item (
                id INTEGER PRIMARY KEY AUTOINCREMENT, 
                title TEXT NOT NULL,
                description TEXT NOT NULL,
                userId INT NOT NULL ,
                imgType TEXT NOT NULL,
                tagId INT NOT NULL,
                willingSwapItem TEXT NOT NULL
                );`
            await this.db.run(sql)
            return this
        })()
    }

    checkItem(values) {
        for (let i = 0; i < values.length; i++) {
            if (values[i].length === 0)
                return false
        }
        return true
    }

    async createItem(title, description, userId, imgType, tagId, willingSwapItem) {
        try {
            // check parameters
            const values = [title, description, userId, imgType, tagId, willingSwapItem]
            if (this.checkItem(values) === false) throw new Error('missing the values')
            const extension = mime.extension(imgType)
            const sql = `INSERT INTO item(title, description, userId, imgType, tagId, willingSwapItem)
                VALUES("${title}", "${description}", "${userId}", "${extension}", "${tagId}", "${willingSwapItem}")`
            const data = await this.db.run(sql)
            return data.lastID
        } catch (err) {
            throw err
        }
    }

    async getItemListByUser(userId) {
        if (userId === null || userId === undefined) throw new Error('parameter userId is error')
        try {
            const sql = `SELECT * FROM item where userId=${userId}`
            const data = await this.db.all(sql)
            // if (!data.length) throw new Error('there is no item')
            return data
        } catch (err) {
            throw err
        }
    }

    async getItemListByTag(tagId) {
        if (tagId === null || tagId === undefined) throw new Error('parameter tagId is error')
        try {
            // tagId === 0 means get all item
            if (tagId === 0) {
                const sql = 'SELECT * FROM item'
                const data = await this.db.all(sql)
                return data
            }
            const sql = `SELECT * FROM item where tagId=${tagId}`
            const data = await this.db.all(sql)
            if (!data.length) throw new Error('there is no item')
            return data
        } catch (err) {
            throw err
        }
    }

    async getItemById(id) {
        if (id === null || id === undefined) throw new Error('parameter itemId is error')
        try {
            const sql = `SELECT * FROM item where id=${id}`
            const data = await this.db.get(sql)
            if (!data) throw new Error('there is no item')
            return data
        } catch (err) {
            throw err
        }
    }

    async uploadPicture(path, mimeType, username) {
        try {
            if (path.length === 0 || path === undefined)
                throw new Error('image path err')
            if (mimeType.length === 0 || mimeType === undefined)
                throw new Error('image format err')
            const extension = mime.extension(mimeType)
            const pic = `public/itemImg/${username}.${extension}`
            const sizeX = 300
            const sizeY = 200
            await Sharp(path).resize(sizeX, sizeY).toFile(pic)
            return true
        } catch (err) {
            throw err
        }
    }
}
