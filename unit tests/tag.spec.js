
'use strict'

const Tag = require('../modules/tag.js')

describe('getItemSwapTagList()', () => {

	test('process get item swap tag list', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		const data = await tag.getItemSwapTagList('1,2,3,4,5')
		expect(data).not.toBeNull()
		done()
	})

	test('parameter undefined', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		await expect(tag.getItemSwapTagList(undefined))
			.rejects.toEqual(Error('parameter swapList is error'))
		done()
	})
})

describe('createTag()', () => {

	test('process to create a new tag', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		const data = await tag.createTag('wei')
		expect(data).not.toBeNull()
		done()
	})

	test('parameter undefined', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		await expect(tag.createTag(undefined))
			.rejects.toEqual(Error('parameter tag is error'))
		done()
	})
})


describe('getTagByName()', () => {

	test('process ', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		const data = await tag.getTagByName('shoes')
		expect(data).not.toBeNull()
		done()
	})

	test('parameter undefined', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		await expect(tag.getTagByName(undefined))
			.rejects.toEqual(Error('parameter tag is error'))
		done()
	})

	test('get a new tag name', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		await tag.createTag('wei')
		const data = await tag.getTagByName('wei')
		expect(data).not.toBeNull()
		done()
	})
})

describe('getTagById()', () => {

	test('process get a tag by tag id', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		const data = await tag.getTagById(1)
		expect(data).not.toBeNull()
		done()
	})

	test('parameter undefined', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		await expect(tag.getTagById(undefined))
			.rejects.toEqual(Error('parameter tagId is error'))
		done()
	})
})

describe('initTags()', () => {

	test('process init tag list for items', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		const data = await tag.initTags()
		expect(data).toBe(true)
		done()
	})

})

describe('getTagList()', () => {

	test('process get a tag list', async done => {
		expect.assertions(1)
		const tag = await new Tag()
		const data = await tag.getTagList()
		expect(data).not.toBeNull()
		done()
	})

})
