
'use strict'

const Email = require('../modules/email.js')

describe('sendEmail()', () => {

	test('missing parameters', async done => {
		expect.assertions(1)
		const email = await new Email()
		await expect(email.sendEmail(undefined))
			.rejects.toEqual(Error('email needs parameters error'))
		done()
	})

	test('process to sent an email', async done => {
		expect.assertions(1)
		//init testing data
		const sentUser = {id: 1, user: 'guow10', email: 'test@gmail.com'}
		const hostUser = {id: 2, user: 'gw', email: '1908282377@qq.com'}
		const item = {
			id: '/item?id=10',
			title: 'wei_phone',
			description: 'testestes',
			userId: 2,
			imgType: 'jpeg',
			tagId: 3,
			willingSwapItem: '1,2,3',
			itemImg: '/itemImg/wei_phone.jpeg'
		  }
		const tags = [
			{ id: 1, tag: 'shoes' },
			{ id: 2, tag: 'sock' },
			{ id: 3, tag: 'pants' }
		  ]
		const email = await new Email()
		const data = await email.sendEmail(sentUser, hostUser, item, 'iphone', tags)
		expect(data).toBe(true)
		done()
	})
})

describe('saveEmailLog()', () => {

	test('save sent email log', async done => {
		expect.assertions(1)
		const email = await new Email()
		const data = await email.saveEmailLog('sentUser', 'hostUser', 0, 1, '1,2,3,4')
		expect(data).not.toBeNull()
		done()
	})

	test('missing parameters', async done => {
		expect.assertions(1)
		const email = await new Email()
		await expect(email.saveEmailLog('sentUser', 'hostUser', 0, 1, ''))
			.rejects.toEqual(Error('missing the values'))
		done()
	})
})
