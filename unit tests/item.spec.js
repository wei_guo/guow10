
'use strict'

const mock = require('mock-fs')
const Item = require('../modules/item.js')

describe('uploadPicture()', () => {
	// this would have to be done by mocking the file system
	// perhaps using mock-fs?

	beforeAll(() => {
		mock({
			'img': mock.directory({
				mode: '0755',
				items: {
					'fake.png': Buffer.from([8, 6, 7, 5, 3, 0, 9])
				}
			})
		})
	})

	afterAll(() => {
		mock.restore()
	})

	test('upload picture if blank path', async done => {
		expect.assertions(1)
		const account = await new Item()
		await expect(account.uploadPicture('', 'image/jpeg', 'test'))
			.rejects.toEqual(Error('image path err'))
		done()
	})

	test('upload picture if error img types', async done => {
		expect.assertions(1)
		const item = await new Item()
		await expect(item.uploadPicture('../public/avatars/avatar.png', '', 'test'))
			.rejects.toEqual(Error('image format err'))
		done()
	})
})

describe('getItemById()', () => {

	test('process get item by id', async done => {
		expect.assertions(1)
		const item = await new Item()
		await item.createItem('iphone8', 'it is a new iphone8', 1, 'image/jpeg', 1, '1,2,3,4,5')
		const data = await item.getItemById(1)
		expect(data).not.toBeNull()
		done()
	})

	test('undefined parameter', async done => {
		expect.assertions(1)
		const item = await new Item()
		await expect(item.getItemById(undefined))
			.rejects.toEqual(Error('parameter itemId is error'))
		done()
	})

	test('no item if get item by id', async done => {
		expect.assertions(1)
		const item = await new Item()
		await expect(item.getItemById(1))
			.rejects.toEqual(Error('there is no item'))
		done()
	})
})

describe('createItem()', () => {

	test('process create an item', async done => {
		expect.assertions(1)
		const item = await new Item()
		const data = await item.createItem('iphone8', 'it is a new iphone8', 1, 'image/jpeg', 1, '1,2,3,4,5')
		expect(data).not.toBeNull()
		done()
	})

	test('missing parameter', async done => {
		expect.assertions(1)
		const item = await new Item()
		await expect(item.createItem('', 'it is a new iphone8', 1, 'image/jpeg', 1, '1,2,3,4,5'))
			.rejects.toEqual(Error('missing the values'))
		done()
	})
})

describe('getItemListByUser()', () => {

	test('process get item list by user id', async done => {
		expect.assertions(1)
		const item = await new Item()
		await item.createItem('iphone8', 'it is a new iphone8', 1, 'image/jpeg', 1, '1,2,3,4,5')
		const data = await item.getItemListByUser(1)
		expect(data).not.toBeNull()
		done()
	})

	test('undefined parameter', async done => {
		expect.assertions(1)
		const item = await new Item()
		await expect(item.getItemListByUser(undefined))
			.rejects.toEqual(Error('parameter userId is error'))
		done()
	})

	// test('there is no item by user id', async done => {
	// 	expect.assertions(1)
	// 	const item = await new Item()
	// 	await expect(item.getItemListByUser(1))
	// 		.rejects.toEqual(Error('there is no item'))
	// 	done()
	// })
})

describe('getItemListByTag()', () => {

	test('process get item by tag id', async done => {
		expect.assertions(1)
		const item = await new Item()
		await item.createItem('iphone8', 'it is a new iphone8', 1, 'image/jpeg', 1, '1,2,3,4,5')
		const data = await item.getItemListByTag(1)
		expect(data).not.toBeNull()
		done()
	})

	test('undefined parameter', async done => {
		expect.assertions(1)
		const item = await new Item()
		await expect(item.getItemListByTag(undefined))
			.rejects.toEqual(Error('parameter tagId is error'))
		done()
	})

	test('there is no item by tag id', async done => {
		expect.assertions(1)
		const item = await new Item()
		await expect(item.getItemListByTag(1))
			.rejects.toEqual(Error('there is no item'))
		done()
	})
})
