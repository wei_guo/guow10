
'use strict'

const mock = require('mock-fs')
const Accounts = require('../modules/user.js')

describe('getUserById()', () => {

	test('get a user by user id', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await account.register('doej', 'password', 'test@uni.coventry.ac.uk')
		const data = await account.getUserById(1)
		expect(data).not.toBeNull()
		done()
	})

	test('get user error if get a user by user id', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await expect(account.getUserById(1))
			.rejects.toEqual(Error('get user by Id error'))
		done()
	})
})

describe('register()', () => {

	test('register a valid account', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		const register = await account.register('doej', 'password', 'test@uni.coventry.ac.uk')
		expect(register).toBe(true)
		done()
	})

	test('register a duplicate username', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await account.register('doej', 'password', 'test@uni.coventry.ac.uk')
		await expect(account.register('doej', 'password', 'test@uni.coventry.ac.uk'))
			.rejects.toEqual(Error('username "doej" already in use'))
		done()
	})

	test('error if blank username', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await expect(account.register('', 'password', 'test@uni.coventry.ac.uk'))
			.rejects.toEqual(Error('missing username'))
		done()
	})

	test('error if blank password', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await expect(account.register('doej', '', 'test@uni.coventry.ac.uk'))
			.rejects.toEqual(Error('missing password'))
		done()
	})

	test('error if blank email', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await expect(account.register('doej', 'password', ''))
			.rejects.toEqual(Error('missing email'))
		done()
	})

})

describe('uploadPicture()', () => {
	// this would have to be done by mocking the file system
	// perhaps using mock-fs?

	beforeEach(() => {
		mock({
			'img': mock.directory({
				mode: '0755',
				items: {
					'fake.png': Buffer.from([8, 6, 7, 5, 3, 0, 9])
				}
			})
		})
	})

	afterEach(() => {
		mock.restore()
	})

	test('upload picture if blank path', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await expect(account.uploadPicture('', 'image/jpeg', 'test'))
			.rejects.toEqual(Error('image path err'))
		done()
	})

	test('upload picture if error img types', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await expect(account.uploadPicture('../public/avatars/avatar.png', '', 'test'))
			.rejects.toEqual(Error('image format err'))
		done()
	})

	// test('process upload a pic', async done => {
	// 	expect.assertions(1)
	// 	const account = await new Accounts()
	// 	const data = await account.uploadPicture('../public/avatars/avatar.png', 'image/png', 'test')
	// 	expect(data).toBe(true)
	// 	done()
	// })
})

describe('login()', () => {
	test('log in with valid credentials', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await account.register('doej', 'password', 'test@uni.coventry.ac.uk')
		const valid = await account.login('doej', 'password')
		// expect(valid).toBe(true)
		expect(valid.id).not.toBeNull()
		done()
	})

	test('invalid username', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await account.register('doej', 'password', 'test@uni.coventry.ac.uk')
		await expect(account.login('roej', 'password'))
			.rejects.toEqual(Error('username "roej" not found'))
		done()
	})

	test('invalid password', async done => {
		expect.assertions(1)
		const account = await new Accounts()
		await account.register('doej', 'password', 'test@uni.coventry.ac.uk')
		await expect(account.login('doej', 'bad'))
			.rejects.toEqual(Error('invalid password for account "doej"'))
		done()
	})
})
