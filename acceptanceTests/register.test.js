
'use strict'

const puppeteer = require('puppeteer')
const { configureToMatchImageSnapshot } = require('jest-image-snapshot')
const PuppeteerHar = require('puppeteer-har')
const shell = require('shelljs')

const width = 800
const height = 600
const delayMS = 5

const baseUrl = 'https://guow10.herokuapp.com:8080'

let browser
let page
let har

// threshold is the difference in pixels before the snapshots dont match
const toMatchImageSnapshot = configureToMatchImageSnapshot({
	customDiffConfig: { threshold: 2 },
	noColors: true,
})
expect.extend({ toMatchImageSnapshot })

beforeAll( async() => {
	browser = await puppeteer.launch({ headless: true, slowMo: delayMS, args: [`--window-size=${width},${height}`] })
	page = await browser.newPage()
	har = new PuppeteerHar(page)
	await page.setViewport({ width, height })
	await shell.exec('acceptanceTests/scripts/beforeAll.sh')
})

afterAll( async() => {
	browser.close()
	await shell.exec('acceptanceTests/scripts/afterAll.sh')
})

beforeEach(async() => {
	await shell.exec('acceptanceTests/scripts/beforeEach.sh')
})

describe('register', () => {

	test('register a duplicate username', async done => {
		//start generating a trace file.
		await page.tracing.start({path: 'trace/duplicate_username_har.json',screenshots: true})
		await har.start({path: 'trace/duplicate_usernamee_trace.har'})
		//ARRANGE
		await page.goto(`${baseUrl}/register`, { timeout: 30000, waitUntil: 'load' })
		//ACT
		await page.screenshot({path: 'screenshots/register/duplicate_username_register_goto1.png'})
		await page.type('input[name=user]', 'NewUser')
		await page.type('input[name=pass]', 'password')
		await page.type('input[name=email]', 'test@coventry.ac.uk')
		await page.type('input[type=file]', 'fake.png')
		await page.screenshot({path: 'screenshots/register/duplicate_username_register_afterType1.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/register/duplicate_username_index_goto1.png'})
		await page.goto(`${baseUrl}/register`, { timeout: 30000, waitUntil: 'load' })
		await page.screenshot({path: 'screenshots/register/duplicate_username_register_goto2.png'})
		await page.type('input[name=user]', 'NewUser')
		await page.type('input[name=pass]', 'password')
		await page.type('input[name=email]', 'test@coventry.ac.uk')
		await page.type('input[type=file]', 'fake.png')
		await page.screenshot({path: 'screenshots/register/duplicate_username_register_afterType2.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/register/duplicate_username_error_goto2.png'})
		//ASSERT
		//check that the user is taken to the error after attempting to register as the new user:
		await page.waitForSelector('h2')
		expect( await page.evaluate( () => document.querySelector('h2').innerText ) )
			.toBe('username "NewUser" already in use')

		// grab a screenshot
		const image = await page.screenshot()
		// compare to the screenshot from the previous test run
		expect(image).toMatchImageSnapshot()
		// stop logging to the trace files
		await page.tracing.stop()
		await har.stop()
		done()
	}, 30000)

	test('error if blank username', async done => {
		//start generating a trace file.
		await page.tracing.start({path: 'trace/blank_username.json', screenshots: true})
		await har.start({path: 'trace/blank_username_trace.har'})
		//ARRANGE
		await page.goto(`${baseUrl}/register`, { timeout: 30000, waitUntil: 'load' })
		//ACT
		await page.screenshot({path: 'screenshots/register/blank_username_register_goto.png'})
		await page.type('input[name=user]', '')
		await page.type('input[name=pass]', 'password')
		await page.type('input[name=email]', 'test@coventry.ac.uk')
		await page.type('input[type=file]', 'fake.png')
		await page.screenshot({path: 'screenshots/register/blank_username_register_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/register/blank_username_error_goto.png'})
		//ASSERT
		//check that the user is taken to the error after attempting to register as the new user:
		await page.waitForSelector('h2')
		expect( await page.evaluate( () => document.querySelector('h2').innerText ) )
			.toBe('missing username')

		// grab a screenshot
		const image = await page.screenshot()
		// compare to the screenshot from the previous test run
		expect(image).toMatchImageSnapshot()
		// stop logging to the trace files
		await page.tracing.stop()
		await har.stop()
		done()
	}, 30000)

	test('error if blank password', async done => {
		//start generating a trace file.
		await page.tracing.start({path: 'trace/blank_password.json',screenshots: true})
		await har.start({path: 'trace/blank_password_trace.har'})
		//ARRANGE
		await page.goto(`${baseUrl}/register`, { timeout: 30000, waitUntil: 'load' })
		//ACT
		await page.screenshot({path: 'screenshots/register/blank_password_register_goto.png'})
		await page.type('input[name=user]', 'NewUser')
		await page.type('input[name=pass]', '')
		await page.type('input[name=email]', 'test@coventry.ac.uk')
		await page.type('input[type=file]', 'fake.png')
		await page.screenshot({path: 'screenshots/register/blank_password_register_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/register/blank_password_error_goto.png'})
		//ASSERT
		//check that the user is taken to the error after attempting to register as the new user:
		await page.waitForSelector('h2')
		expect( await page.evaluate( () => document.querySelector('h2').innerText ) )
			.toBe('missing password')

		// grab a screenshot
		const image = await page.screenshot()
		// compare to the screenshot from the previous test run
		expect(image).toMatchImageSnapshot()
		// stop logging to the trace files
		await page.tracing.stop()
		await har.stop()
		done()
	}, 30000)

	test('error if blank email', async done => {
		//start generating a trace file.
		await page.tracing.start({path: 'trace/blank_email.json',screenshots: true})
		await har.start({path: 'trace/blank_email_trace.har'})
		//ARRANGE
		await page.goto(`${baseUrl}/register`, { timeout: 30000, waitUntil: 'load' })
		//ACT
		await page.screenshot({path: 'screenshots/register/blank_email_register_goto.png'})
		await page.type('input[name=user]', 'NewUser')
		await page.type('input[name=pass]', 'password')
		await page.type('input[name=email]', '')
		await page.type('input[type=file]', 'fake.png')
		await page.screenshot({path: 'screenshots/register/blank_email_register_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/register/blank_email_error_goto.png'})
		//ASSERT
		//check that the user is taken to the error after attempting to register as the new user:
		await page.waitForSelector('h2')
		expect( await page.evaluate( () => document.querySelector('h2').innerText ) )
			.toBe('missing email')

		// grab a screenshot
		const image = await page.screenshot()
		// compare to the screenshot from the previous test run
		expect(image).toMatchImageSnapshot()
		// stop logging to the trace files
		await page.tracing.stop()
		await har.stop()
		done()
	}, 30000)

})
