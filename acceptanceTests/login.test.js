
'use strict'

const puppeteer = require('puppeteer')
const { configureToMatchImageSnapshot } = require('jest-image-snapshot')
const PuppeteerHar = require('puppeteer-har')
const shell = require('shelljs')

const width = 800
const height = 600
const delayMS = 5

const baseUrl = 'https://guow10.herokuapp.com:8080'

let browser
let page
let har

// threshold is the difference in pixels before the snapshots dont match
const toMatchImageSnapshot = configureToMatchImageSnapshot({
	customDiffConfig: { threshold: 2 },
	noColors: true,
})
expect.extend({ toMatchImageSnapshot })

beforeAll( async() => {
	browser = await puppeteer.launch({ headless: true, slowMo: delayMS, args: [`--window-size=${width},${height}`] })
	page = await browser.newPage()
	har = new PuppeteerHar(page)
	await page.setViewport({ width, height })
	await shell.exec('acceptanceTests/scripts/beforeAll.sh')
})

afterAll( async() => {
	browser.close()
	await shell.exec('acceptanceTests/scripts/afterAll.sh')
})

beforeEach(async() => {
	await shell.exec('acceptanceTests/scripts/beforeEach.sh')
})

describe('logging in', () => {

	test('User logging in', async done => {
		//start generating a trace file.
		await page.tracing.start({path: 'trace/login_har.json',screenshots: true})
		await har.start({path: 'trace/login_trace.har'})
		//ARRANGE
		await page.goto(`${baseUrl}/register`, { timeout: 30000, waitUntil: 'load' })
		//ACT
		await page.screenshot({path: 'screenshots/login/User_logging_in_register_goto.png'})
		await page.type('input[type=text]', 'NewUser')
		await page.type('input[type=password]', 'password')
		await page.type('input[type=email]', 'test@coventry.ac.uk')
		await page.type('input[type=file]', 'fake.png')
		await page.screenshot({path: 'screenshots/login/User_logging_in_register_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/login/User_logging_in_register_submit.png'})
		await page.goto(`${baseUrl}/login`, { timeout: 30000, waitUntil: 'load' })
		await page.screenshot({path: 'screenshots/login/User_logging_in_login_goto.png'})
		await page.type('input[type=text]', 'NewUser')
		await page.type('input[type=password]', 'password')
		await page.screenshot({path: 'screenshots/login/User_logging_in_login_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/login/User_logging_in_index_goto.png'})
		//ASSERT
		//check that the user is taken to the homepage after attempting to login as the new user:
		await page.waitForSelector('title')
		expect( await page.evaluate( () => document.querySelector('title').innerText ) )
			.toBe('Exchange')

		// grab a screenshot
		const image = await page.screenshot()
		// compare to the screenshot from the previous test run
		expect(image).toMatchImageSnapshot()
		// stop logging to the trace files
		await page.tracing.stop()
		await har.stop()
		done()
	}, 30000)

	test('invalid username', async done => {
		//start generating a trace file.
		await page.tracing.start({path: 'trace/invalid_username_har.json',screenshots: true})
		await har.start({path: 'trace/invalid_username_trace.har'})
		//ARRANGE
		await page.goto(`${baseUrl}/register`, { timeout: 30000, waitUntil: 'load' })
		//ACT
		await page.screenshot({path: 'screenshots/login/invalid_username_register_goto.png'})
		await page.type('input[type=text]', 'NewUser')
		await page.type('input[type=password]', 'password')
		await page.type('input[type=email]', 'test@coventry.ac.uk')
		await page.type('input[type=file]', 'fake.png')
		await page.screenshot({path: 'screenshots/login/invalid_username_register_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/login/invalid_username_register_submit.png'})
		await page.goto(`${baseUrl}/login`, { timeout: 30000, waitUntil: 'load' })
		await page.screenshot({path: 'screenshots/login/invalid_username_login_goto.png'})
		await page.type('input[type=text]', 'invalid_username')
		await page.type('input[type=password]', 'password')
		await page.screenshot({path: 'screenshots/login/invalid_username_login_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/login/invalid_username_error_goto.png'})
		//ASSERT
		//check that the user is taken to the error after attempting to login as the new user:
		await page.waitForSelector('h2')
		expect( await page.evaluate( () => document.querySelector('h2').innerText ) )
			.toBe('username "invalid_username" not found')

		// grab a screenshot
		const image = await page.screenshot()
		// compare to the screenshot from the previous test run
		expect(image).toMatchImageSnapshot()
		// stop logging to the trace files
		await page.tracing.stop()
		await har.stop()
		done()
	}, 30000)

	test('invalid password', async done => {
		//start generating a trace file.
		await page.tracing.start({path: 'trace/invalid_password.json', screenshots: true})
		await har.start({path: 'trace/invalid_password_trace.har'})
		//ARRANGE
		await page.goto(`${baseUrl}/register`, { timeout: 30000, waitUntil: 'load' })
		//ACT
		await page.screenshot({path: 'screenshots/login/invalid_password_register_goto.png'})
		await page.type('input[type=text]', 'NewUser')
		await page.type('input[type=password]', 'password')
		await page.type('input[type=email]', 'test@coventry.ac.uk')
		await page.type('input[type=file]', 'fake.png')
		await page.screenshot({path: 'screenshots/login/invalid_password_register_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/login/invalid_password_register_submit.png'})
		await page.goto(`${baseUrl}/login`, { timeout: 30000, waitUntil: 'load' })
		await page.screenshot({path: 'screenshots/login/invalid_password_login_goto.png'})
		await page.type('input[type=text]', 'NewUser')
		await page.type('input[type=password]', 'invalid_passwd')
		await page.screenshot({path: 'screenshots/login/invalid_password_login_afterType.png'})
		await page.click('input[type=submit]')
		await page.screenshot({path: 'screenshots/login/invalid_password_error_goto.png'})
		//ASSERT
		//check that the user is taken to the error after attempting to login as the new user:
		await page.waitForSelector('h2')
		expect( await page.evaluate( () => document.querySelector('h2').innerText ) )
			.toBe('invalid password for account "NewUser"')

		// grab a screenshot
		const image = await page.screenshot()
		// compare to the screenshot from the previous test run
		expect(image).toMatchImageSnapshot()
		// stop logging to the trace files
		await page.tracing.stop()
		await har.stop()
		done()
	}, 30000)


})
