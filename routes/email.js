#!/usr/bin/env node

'use strict'

//user Routes File

const Router = require('koa-router')
const Email = require('../modules/email')
const Item = require('../modules/item')
const Tag = require('../modules/tag')
const User = require('../modules/user')
// const Moment = require('moment')
const router = new Router()
const dbName = 'website.db'

/**
 * The Item page.
 *
 * @name Make an offer
 * @route {POST} /offer
 * @authentication This route requires cookie-based authentication.
 */
router.post('/offer', async ctx => {
    // check out cookie-based authentication
    if (ctx.session.authorised === true) {
        // extract the data from the request
        const body = ctx.request.body
        if (!body.itemId) await ctx.render('error', { message: 'Item id is error' })
        if (!body.willingItem) await ctx.render('error', { message: 'input error' })
        const willingItem = body.willingItem
        try {
            // call the functions in the module
            const item = await new Item(dbName)
            // check item value
            const itemData = await item.getItemById(body.itemId)
            if (!itemData) await ctx.render('error', { message: 'there is no item' })
            // init Data to send an email
            itemData.itemImg = `/itemImg/${itemData.title}.${itemData.imgType}`
            itemData.id = `/item?id=${itemData.id}`
            const user = await new User(dbName)
            const hostUser = await user.getUserById(itemData.userId)
            const sentUser = await user.getUserById(ctx.session.userId)
            const tag = await new Tag(dbName)
            const tagData = await tag.getItemSwapTagList(itemData.willingSwapItem)
            // send an email
            const email = await new Email(dbName)
            const resEmail = email.sendEmail(sentUser, hostUser, itemData, willingItem, tagData)
            if (!resEmail) await ctx.render('error', { message: 'sent error' })
            // redner / page and send an response
            await ctx.redirect('/', { message: 'you have sent an offer to the item host', resEmail: false })
        } catch (err) {
            await ctx.render('error', { message: err.message })
        }
    } else
        // redirect to the login page
        await ctx.redirect('/login?msg=you are not logged in...')
})

/**
 * The Item page.
 *
 * @name Make an offer
 * @route {GET} /offer
 * @authentication This route requires cookie-based authentication.
 */
router.get('/offer', async ctx => {
    await ctx.render('error', { message: 'cannot use {get} to send an email, server error' })
})

module.exports = router
