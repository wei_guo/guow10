#!/usr/bin/env node

'use strict'

//user Routes File

const Router = require('koa-router')
const User = require('../modules/user')
const Item = require('../modules/item')
const router = new Router()
const dbName = 'website.db'

/**
 * The user registration page.
 *
 * @name Register Page
 * @route {GET} /register
 */
router.get('/register', async ctx => await ctx.render('register'))

/**
 * The script to check emial validation.
 *
 * @name IsEmail Script
 * @function check email validation
 */
function IsEmail(email) {
    const reg = /^\w+@[a-zA-Z0-9]{2,10}(?:\.[a-z]{2,4}){1,3}$/
    return reg.test(email)
}

/**
 * The script to process new user registrations.
 *
 * @name Register Script
 * @route {POST} /register
 */
router.post('/register', async ctx => {
    try {
        // extract the data from the request
        const body = ctx.request.body
        if (ctx.request.files === null || ctx.request.files === undefined)
            await ctx.render('error', { message: 'upload file error' })
        // remove space of the data
        body.user=body.user.replace(/\s+/g, '')
        const fileJson = JSON.parse(JSON.stringify(ctx.request.files.avatar))
        const avatarsFilePath = fileJson.path
        const filetype = fileJson.type
        //check email from the data from
        if(!IsEmail(body.email))
            await ctx.render('error', { message: 'invalid email address' })
        // call the functions in the module
        const user = await new User(dbName)
        await user.register(body.user, body.pass, body.email)
        await user.uploadPicture(avatarsFilePath, filetype, body.user)
        // redirect to the home page
        await ctx.redirect(`/?msg=new user "${body.name}" added`)
    } catch (err) {
        await ctx.render('error', { message: err.message })
    }
})

/**
 * The user login page
 *
 * @name Login page
 * @route {GET} /login
 */
router.get('/login', async ctx => {
    const data = {}
    if (ctx.query.msg) data.msg = ctx.query.msg
    if (ctx.query.user) data.user = ctx.query.user
    await ctx.render('login', data)
})

/**
 * The user page
 *
 * @name User page
 * @route {GET} /User?id=
 * @authentication This route requires cookie-based authentication.
 */
router.get('/user', async ctx => {
    if (ctx.session.authorised === true) {
        // extract the data from the request
        const userId = JSON.stringify(ctx.query.id)
        if (userId === null || userId === undefined) await ctx.render('error', { message: 'userId null or undefined' })
        try {
            // call the functions in the module
            const user = await new User(dbName)
            const data = await user.getUserById(userId)
            if (!data) await ctx.render('error', { message: 'there is no user' })
            const item = await new Item(dbName)
            const itemList = await item.getItemListByUser(userId)
            //init user page data
            if (itemList.length !== 0) {
                for (let i = 0; i < itemList.length; i++) {
                    itemList[i].itemImg = `/itemImg/${itemList[i].title}.${itemList[i].imgType}`
                    itemList[i].id = `/item?id=${itemList[i].id}`
                }
            }
            await ctx.render('user', { authorised: ctx.session.authorised, data, itemList })
        } catch (err) {
            await ctx.render('error', { message: err.message })
        }
    } else
    // redirect to the login page
    await ctx.redirect('/login?msg=you are not logged in...')
})

/**
 * The script to process login.
 *
 * @name Login Script
 * @route {POST} /login
 */
router.post('/login', async ctx => {
    try {
        // extract the data from the request
        const body = ctx.request.body
        // call the functions in the module
        const user = await new User(dbName)
        const userId = await user.login(body.user, body.pass)
        // set up session authorised
        ctx.session.authorised = true
        ctx.session.userId = userId.id
        // redirect to the home page
        await ctx.redirect('/?msg=you are now logged in...', { resEmail: true })
    } catch (err) {
        await ctx.render('error', { message: err.message })
    }
})

/**
 * The user logout script
 *
 * @name Logout Script
 * @route {GET} /logout
 */
router.get('/logout', async ctx => {
    // remove session authorised
    ctx.session.authorised = null
    ctx.session.userId = null
    // redirect to the home page
    return ctx.redirect('/?msg=you are now logged out', { resEmail: false })
})

module.exports = router
