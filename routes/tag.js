#!/usr/bin/env node

'use strict'

//user Routes File

const Router = require('koa-router')
const Tag = require('../modules/tag')
const Item = require('../modules/item')
const router = new Router()
const dbName = 'website.db'

/**
 * The search result page.
 *
 * @name result Page
 * @route {GET} /tag
 */
router.get('/tag', async ctx => {
    try {
        // extract the data from the request
        const body = ctx.query
        if (body.tag === null || body.tag === undefined) await ctx.redirect('/?msg=null', { resEmail: false })
        // remove space of the data
        body.tag=body.tag.replace(/\s+/g, '')
        // call the functions in the module
        const tag = await new Tag(dbName)
        // check tag from database
        const tagData = await tag.getTagByName(body.tag)
        if (!tagData) await ctx.redirect('/?msg=there is no tag', { resEmail: false })
        // get item by tag name
        const items = await new Item(dbName)
        const itemList = await items.getItemListByTag(tagData.id)
        // get data to init result page
        for (let i = 0; i < itemList.length; i++) {
            itemList[i].itemImg = `/itemImg/${itemList[i].title}.${itemList[i].imgType}`
            itemList[i].id = `/item?id=${itemList[i].id}`
        }
        //set up ctx for image
        ctx.type = 'image/png'
        ctx.status = 200
        // render result page
        await ctx.render('result', { authorised: ctx.session.authorised, itemList })
    } catch (err) {
        await ctx.render('error', { message: err.message })
    }
})

module.exports = router
