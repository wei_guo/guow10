#!/usr/bin/env node

'use strict'

//user Routes File

const Router = require('koa-router')
const Item = require('../modules/item')
const Tag = require('../modules/tag')
const User = require('../modules/user')
const router = new Router()
const dbName = 'website.db'

/**
 * The user create Item page.
 *
 * @name Create item Page
 * @route {GET} /create
 * @authentication This route requires cookie-based authentication.
 */
router.get('/create', async ctx => {
    // check out cookie-based authentication
    if (ctx.session.authorised === true) {
        // extract the data from the request
        const tagId = JSON.stringify(ctx.query.id)
        // if tagId is 0, means user will make a new tag
        if (tagId === undefined) tagId = 0
        let tag = { tag: '' }
        try {
            // call the functions in the module
            const tags = await new Tag(dbName)
            if (tagId !== 0) {
                // for showing the item what user making
                tag = await tags.getTagById(tagId)
                if (!tag) await ctx.render('error', { message: 'there is no tag, server error' })
            }
            // for showing user to make tag what they want to swap
            const tagList = await tags.getTagList()
            if (tagList.length === 0) await ctx.render('error', { message: 'there is no tags list, server error' })
            // redner createItem page
            await ctx.render('create', {
                authorised: ctx.session.authorised,
                tag: tag, tagList,
                userId: `user?id=${ctx.session.userId}`
            })
        } catch (err) {
            await ctx.render('error', { message: err.message })
        }
    } else
        // redirect to the login page
        await ctx.redirect('/login?msg=you are not logged in...')
})

/**
 * The script to values data form by post /create
 *
 * @name create Item Page
 * @function this function checks values from a form by post /create
 */
function checkValues(valuesForm) {
    for (let i = 0; i < valuesForm.length; i++) {
        if (valuesForm[i] === null || valuesForm[i] === undefined)
            return false
    }
    return true
}

/**
 * The script to process a new item.
 *
 * @name Create item script
 * @route {POST} /create
 * @authentication This route requires cookie-based authentication.
 */
router.post('/create', async ctx => {
    // check out cookie-based authentication
    if (ctx.session.authorised === true) {
        // extract the data from the request
        const body = ctx.request.body
        const fileJson = JSON.parse(JSON.stringify(ctx.request.files.itemImg))
        const valuesForm = [
            body.title, body.description,
            ctx.session.userId,
            fileJson.type,
            body.tag,
            body.willingSwapItem
        ]
        // check values from form
        if (checkValues(valuesForm) === false) await ctx.render('error', { message: 'value err' })
        // remove space of the data
        body.title=body.title.replace(/\s+/g, '')
        try {
            //check the tag, if tagId is new, shuold create a new tagId
            const tag = await new Tag(dbName)
            const tagData = await tag.getTagByName(body.tag)
            let tagId = 0
            if (!tagData) tagId = await tag.createTag(body.tag)
            else tagId = tagData.id
            // call the functions in the module
            const newItem = await new Item(dbName)
            const data = await newItem.createItem(
                body.title, body.description,
                ctx.session.userId,
                fileJson.type,
                tagId,
                body.willingSwapItem
                )
            if (data) {
                // if create item successful, the item img will upload and save on server
                await newItem.uploadPicture(fileJson.path, fileJson.type, body.title)
                // redirect to the home page
                await ctx.redirect(`/?msg=new itme "${body.title}" added`)
            }
        } catch (err) {
            await ctx.render('error', { message: err.message })
        }
    } else
        // redirect to the login page
        await ctx.redirect('/login?msg=you are not logged in...')
})

/**
 * The user create Item page.
 *
 * @name Item Detials Page
 * @function This function to make the user information to have GDPR complicance
 * @GDPR GDPR complicance for item details page
 */
function userInfo(user) {
    const ret = user
    const hiddenStart = 2
    ret.user = `Mr/Ms *** ${ret.user.substring(user.user.length - hiddenStart/user.user.length, user.user.length)}`
    ret.email = `***@ ${ret.email.split('@')[1]}`
    return ret
}

/**
 * The Item Detials page.
 *
 * @name Item Detials Page
 * @route {GET} /:id
 * @authentication This route requires cookie-based authentication.
 * @GDPR complicance for item details page
 */
router.get('/item', async ctx => {
    // check out cookie-based authentication
    if (ctx.session.authorised === true) {
        // extract the data from the request
        const itemId = ctx.query.id
        //if tagId is 0, means user will make a new tag
        // check request data
        if (itemId === null || itemId === undefined) await ctx.render('error', { message: 'Item id err' })
        try {
            // call the functions in the module
            const item = await new Item(dbName)
            const data = await item.getItemById(itemId)
            // check data from database
            if (!data) await ctx.render('error', { message: 'There is no item' })
            // init the data for item details page
            const itemData = {
                title: data.title ? data.title : await ctx.render('error', { message: 'init item title error' }),
                itemId: itemId,
                tag: '',
                description: data.description ?
                    data.description : await ctx.render('error', { message: 'init item title error' }),
                user: '',
                itemImg: `/itemImg/${data.title}.${data.imgType}`,
                swapForItems: '',
            }
            // get the item tag name for showing
            const tag = await new Tag(dbName)
            itemData.tag = await tag.getTagById(data.tagId)
            // init host user name
            const user = await new User(dbName)
            itemData.user = await user.getUserById(data.userId)
            // init the tag what host user willing swap for
            itemData.swapForItems = await tag.getItemSwapTagList(data.willingSwapItem)
            // GDPR compliance for item details page
            itemData.user = userInfo(itemData.user)
            // render item details page
            await ctx.render('item', {
                authorised: ctx.session.authorised,
                itemData, userId: `user?id=${ctx.session.userId}`
            })
            } catch (err) {
                await ctx.render('error', { message: err.message })
            }
    } else
        // redirect to the login page
        await ctx.redirect('/login?msg=you are not logged in...')
})

module.exports = router
